####################
### Build Stage  ###
####################
FROM mavenqa.got.volvo.net:18443/library/node as builder

# Set working directory
RUN mkdir /usr/src/app
WORKDIR /usr/src/app

ENV PATH /usr/src/app/node_modules/.bin:$PATH

# Install and cache app dependencies
COPY package.json /usr/src/app/package.json
RUN npm config set https-proxy http://httppxgot.srv.volvo.com:8080
RUN npm install
RUN npm install -g @angular/cli@latest --unsafe

# Copy source
COPY ./ /usr/src/app

# Compile
RUN npm run build --prod

# Start app (only used for testing) without Deploy Stage
#CMD ng serve --host 0.0.0.0

#####################
### Deploy Stage  ###
#####################
FROM mavenqa.got.volvo.net:18443/library/nginx:alpine

#Create folders and set correct permissions for OpenShift
RUN mkdir -p /var/run/nginx /var/log/nginx /var/cache/nginx && \
	chown -R nginx:0 /var/run/nginx /var/log/nginx /var/cache/nginx && \
	chmod -R g=u /var/run/nginx /var/log/nginx /var/cache/nginx

#Copy Configuration files
COPY nginx.conf /etc/nginx/nginx.conf
COPY default.conf /etc/nginx/conf.d/default.conf
#Copy Entrypoint
COPY bin/uid_entrypoint /bin/uid_entrypoint

#Copy application
COPY --from=builder /usr/src/app/dist /usr/share/nginx/html

ENV PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

#OpenShift requires a non-root user.

USER nginx:nginx
EXPOSE 8080

ENTRYPOINT ["/bin/uid_entrypoint"]
CMD ["nginx","-g","daemon off;"]

#Does not work on OpenShift
#RUN rm -rf /usr/share/nginx/html/*


#COPY /nginx.conf /etc/nginx/nginx.conf


# run nginx
#CMD ["nginx", "-g", "daemon off;"]

#to test locally in Docker: Docker run -p 8080:8080 myangularapp