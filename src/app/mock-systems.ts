import { System } from './system';

export const SYSTEMS: System[] = [
  { name: 'SEWS 1' },
  { name: 'SEWS 2' },
  { name: 'Bucket' },
  { name: 'Tea Core' },
  { name: 'SW Configurator' },
  { name: 'RPD' },
  { name: 'PartsCap' }
];