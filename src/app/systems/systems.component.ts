import { Component, OnInit } from '@angular/core';
import { System } from '../system'
import { SystemService } from '../system.service';

@Component({
  selector: 'app-systems',
  templateUrl: './systems.component.html',
  styleUrls: ['./systems.component.css']
})
export class SystemsComponent implements OnInit {

  systems: string[];

  constructor(private systemService: SystemService) { }

  getSystems(): void {
    //this.systemService.getSystemsList();
    this.systemService.getSystems().subscribe(systems => this.systems = systems);
  }

  ngOnInit() {
    this.getSystems();
  }

}
